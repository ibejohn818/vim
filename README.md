John Hardy's VIM dot files
============================

This is my personal traveling VIM/NVIM configuration.

It contains all the plugins that I use referenced as git submodules

and also my .vimrc customizations over the years.

I do heavy development on Linux systems using primarily MAC OS X and Linux Mint/Ubuntu

The main languages I focus on are PHP, JAVSCRIPT, HTML, CSS, BASH/ZSH, PYTHON, ANSIBLE-YAML

My configuration is taylored for 24bit True color terminals but should work on 16bit and 256color terms as well

I use pathogen for plugin configuration.

Here are the plugins, colorschemse and lang packs currently deployed in this repo

Plugins
----------------

- Pathogen
- NERDTree
- Sytastic
- KWDB
- Narrow Region
- ZoomWin
- ACK
- csapprox
- CtrlP
- Gist VIM
- gUNDO
- MatchIT
- NERDCommenter
- Sparkup ( Emmet for VIM But Better )
- SuperTAB
- TAGBAR ( Needs exuberant ctags installed )
- TAGBAR-PHPCTAGS
- TLIB
- MW Utils
- Airline
- Bufferagator
- EasyMotion
- Endwise
- Eunuch
- FuGITtive
- Git Gutter
- Indent Object
- Multiple Cursor
- Repeat
- Sensible
- Snipmate 
- Snipmate Snippets
- Surround
- Trailing Whitespace
- UnimPAIRed
- Visual Star Search
- VRoom
- WEBAPI ( for Gist VIM )
- Dockerfile syntax


